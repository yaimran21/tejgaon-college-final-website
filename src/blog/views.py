from django.shortcuts import render, get_object_or_404,redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
from .models import Post
from .forms import PostForm


def post_list_view(request):
    list_objects = Post.Published.all()
    paginator = Paginator(list_objects, 10)
    page = request.GET.get('page')
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)
    return render(request, 'blog/post/list.html', {'posts': posts})


def post_detail_view(request, year, month, day, slug):
    post = get_object_or_404(Post, slug=slug, publish__year=year, publish__month=month, publish__day=day)
    context ={
        'post': post
    }
    return render(request, 'blog/post/detail.html',context)


def post_create(request):
    form = PostForm(request.POST or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.author = request.user
        instance.save()
    context = {
        "form": form,
    }
    return render(request, 'blog/post/add.html', context)


def post_update(request, year, month, day, slug):
    instance = get_object_or_404(Post, slug=slug, publish__year=year, publish__month=month, publish__day=day)
    form = PostForm(request.POST or None, instance=instance)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
    context ={
        'instance': instance,
        'form': form
    }
    messages.success(request, 'Successfully updated')
    return render(request, 'blog/post/update.html',context)


def post_delete(request, year, month, day, slug):
    post = get_object_or_404(Post, slug=slug, publish__year=year, publish__month=month, publish__day=day)
    if post.author == request.user:
        post.delete()
        messages.success(request, 'Successfully deleted')
        return redirect('blog:post_list_view')
