from django.conf.urls import url
from . import views
from .feeds import PostFeed

app_name = 'blog'

urlpatterns = [
    url(r'^$', views.post_list_view, name='post_list_view'),
    url(r'^create/$', views.post_create, name='post_create'),
    url(r'^(?P<year>[0-9]{4})/(?P<month>[0-9]{2})/(?P<day>[0-9]{2})/(?P<slug>[-\w]+)$',
        views.post_detail_view, name= 'post_detail_view'),
    url(r'^(?P<year>[0-9]{4})/(?P<month>[0-9]{2})/(?P<day>[0-9]{2})/(?P<slug>[-\w]+)/edit/$',
        views.post_update, name='post_edit'),
    url(r'^(?P<year>[0-9]{4})/(?P<month>[0-9]{2})/(?P<day>[0-9]{2})/(?P<slug>[-\w]+)/delete/$',
        views.post_delete, name='post_delete'),

    url(r'^feed/$', PostFeed(), name='post_feed'),

]