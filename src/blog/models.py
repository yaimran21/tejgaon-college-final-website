from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

# Custom Manager


class PublishedManager(models.Manager):
    def get_queryset(self):
        return super(PublishedManager, self).get_queryset().filter(status='published')


# Our Post Model

class Post(models.Model):
    STATUS_CHOICES = (
        ('draft', 'Draft'),
        ('published', 'Published'),
    )
    title = models.CharField(max_length=250)
    slug = models.SlugField(max_length=250, unique_for_date='publish')
    author = models.ForeignKey(User, related_name='blog_posts')
    content = models.TextField()
    upload_image = models.ImageField(blank=True, null=True,height_field='height_field', width_field='width_field', upload_to='blog')
    height_field = models.IntegerField(blank=True, null=True)
    width_field = models.IntegerField(blank=True, null=True)
    publish = models.DateTimeField(default=timezone.now)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='draft')

    # The default manager
    objects = models.Manager()

    # # Custom made manager
    Published = PublishedManager()

    class Meta:
        ordering = ('-publish',)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('blog:post_detail_view', args=[self.publish.year, self.publish.strftime('%m'), self.publish.strftime('%d'), self.slug])

    def edit(self):
        return reverse('blog:post_edit', args=[self.publish.year, self.publish.strftime('%m'), self.publish.strftime('%d'), self.slug])

    def destroy(self):
        return reverse('blog:post_delete', args=[self.publish.year, self.publish.strftime('%m'), self.publish.strftime('%d'), self.slug])