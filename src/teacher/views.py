from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render
from .models import Teacher

# Create your views here.


def index(request):
    teachers = Teacher.objects.all().order_by('-updated_at')
    paginator = Paginator(teachers, 4) # Show 25 contacts per page
    page = request.GET.get('page')
    try:
        teachers = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        teachers = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        teachers = paginator.page(paginator.num_pages)

    context = {
            'teachers': teachers
    }
    return render(request,'teacher/index.html', context)


def view(request, slug):
    teacher = Teacher.objects.get(slug=slug)
    context = {
        'teacher': teacher,

    }
    return render(request, 'teacher/view.html', context)

