from django.contrib import admin
from teacher.models import Teacher

# Register your models here.


class TeacherAdmin(admin.ModelAdmin):
    list_display = ('name', 'email','department','phone_number','date_of_birth')
    search_fields = ('name','email','phone_number')
    list_filter = ('name', 'email')



admin.site.register(Teacher,TeacherAdmin)