from django.db import models
from department.models import Department
from django.template.defaultfilters import slugify
from django.core.validators import URLValidator



# Create your models here.
class Teacher(models.Model):
    designation_type = (
        ('Lecturer', 'Lecturer'),
        ('Professor', 'Professor'),
        ('A. Professor', 'Assistant Professor'),
    )
    # status_type = (
    #     ('Professional', 'Professional'),
    #     ('General', 'General')
    # )
    dept_head =(
        ('yes','yes'),
        ('no','no')
    )

    name = models.CharField(max_length=150)
    email = models.EmailField(max_length=150)
    phone_number = models.CharField(max_length=20)
    facebook_profile = models.URLField(blank=True, null=True, validators=[URLValidator()])

    designation = models.CharField(max_length=50, choices=designation_type,)
    department_head = models.CharField(choices=dept_head, max_length= 10)
    department = models.ForeignKey(Department,on_delete=models.CASCADE, related_name='departments')
    studied_at = models.CharField(max_length=200)
    slug = models.SlugField(editable=False)
    about_teacher = models.TextField(max_length=5000)
    upload_image = models.ImageField(height_field='height_field', width_field='width_field',upload_to='img')
    height_field = models.IntegerField(default=750)
    width_field = models.IntegerField(default=1000)
    date_of_birth = models.DateField(auto_now=False, auto_now_add=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.slug

    def save(self):
        self.slug = '%s' % (
            slugify(self.name)
        )
        super(Teacher, self).save()