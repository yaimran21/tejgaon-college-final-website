from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render
from .models import Department
from teacher.models import Teacher

# Create your views here.

def index(request):
    departments = Department.objects.all().order_by('-created_at')
    paginator = Paginator(departments, 4) # Show 25 contacts per page
    page = request.GET.get('page')
    try:
        departments = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        departments = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        departments = paginator.page(paginator.num_pages)

    context = {
            'departments': departments
    }
    return render(request,'department/index.html', context)

def view(request, slug, pk):
    department= Department.objects.get(slug=slug)
    teachers = Teacher.objects.filter(department = pk)

    contex = {
        'department': department,
        'teachers': teachers
    }
    return render(request, 'department/view.html', contex)

