from django.db import models
from django.template.defaultfilters import slugify


# Create your models here.


class Department(models.Model):
    department = (
        ('CSE', 'CSE'),
        ('BBA', 'BBA'),
        ('EEE', 'EEE'),
        ('Bangla', 'Bangla'),
        ('English', 'English'),
        ('Physics', 'Physics'),
    )
    dept_type = (
        ('Professional', 'Professional'),
        ('General', 'General')
    )
    popular_status = (
        ('Yes','Yes'),
        ('No', 'No')
    	)

    name = models.CharField(max_length=10, choices=department)
    code = models.CharField(max_length=10)
    slug = models.SlugField(editable=False)
    department_type = models.CharField(max_length=50, choices=dept_type)
    popular = models.CharField(max_length=10, choices = popular_status)
    details = models.TextField(max_length=1000)
    height = models.IntegerField(default=750)
    width = models.IntegerField(default=1000)
    upload_image = models.ImageField(height_field='height', width_field='width',upload_to='img/department')
    syllabus = models.FileField( upload_to= 'syllabus')

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name
    def __unicode__(self):
        return self.slug

    def save(self):
        self.slug = '%s' % (
            slugify(self.name)
        )
        super(Department, self).save()


class Course(models.Model):

    semester_number = (
        ('1st', '1st'),
        ('2nd', '2nd'),
        ('3rd', '3rd'),
        ('4th', '4th'),
        ('5th', '5th'),
        ('6th', '6th'),
        ('7th', '7th'),
        ('8th', '8th'),
    )
    year = (
        ('1st', '1st'),
        ('2nd', '2nd'),
        ('3rd', '3rd'),
        ('4th', '4th'),

    	)

    name = models.CharField(max_length=200)
    code = models.CharField(max_length=20)
    slug = models.SlugField(editable=False)
    department = models.ForeignKey(Department,on_delete=models.CASCADE, related_name='department')
    year = models.CharField(max_length=50, choices=year)

    semester = models.CharField(max_length=50, choices=semester_number, blank=True)
    credit = models.CharField(max_length=5, default= 3)
    details = models.TextField(max_length=5000)
    height = models.IntegerField(default= 750)
    width = models.IntegerField(default=1000)
    upload_image = models.ImageField(height_field='height', width_field='width',upload_to='img')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.slug

    def save(self):
        self.slug = '%s' % (
            slugify(self.name)
        )
        super(Course, self).save()