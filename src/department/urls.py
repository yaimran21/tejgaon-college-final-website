from django.conf.urls import url
from . import views

app_name='department'

urlpatterns = [
    url(r'^$',views.index, name='index' ),
    url(r'^(?P<pk>[0-9]+)/(?P<slug>[-\w]+)-department$',views.view, name = 'view'),



]