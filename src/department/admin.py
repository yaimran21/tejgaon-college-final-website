from django.contrib import admin
from department.models import Department
from department.models import Course

# Register your models here.


class DepartmentAdmin(admin.ModelAdmin):
    list_display = ('name', 'code','department_type')
    search_fields = ('name','code','department_type')
    list_filter = ('name', 'code','department_type')


class CourseAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'department', 'semester')
    search_fields = ('name', 'code', 'department', 'semester')
    list_filter = ('name', 'code', 'department', 'semester')


admin.site.register(Department,DepartmentAdmin)
admin.site.register(Course,CourseAdmin)