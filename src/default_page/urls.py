from django.conf.urls import url
from . import views

app_name='default_page'

urlpatterns = [
    url(r'^$', views.home, name='home_page' ),
    url(r'^about-us/$', views.about, name='about_us' ),
    url(r'^about-us/(?P<slug>[-\w]+)$', views.about_us_view, name='about_us_single_view.html' ),
    url(r'^(?P<slug>[-\w]+)/$', views.view, name='view'),


]	