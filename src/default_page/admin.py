from django.contrib import admin
from .models import AboutCollege,GoverningBody


# Register your models here.
class AboutCollegeAdmin(admin.ModelAdmin):
    list_display = ('title','slug', 'created_at')
    search_fields = ('title', 'created_at')
    list_filter = ('title', 'created_at')


class GoverningBodyAdmin(admin.ModelAdmin):
    list_display = ('name','slug', 'created_at')
    search_fields = ('name', 'created_at')
    list_filter = ('name', 'created_at')


admin.site.register(GoverningBody,GoverningBodyAdmin)
admin.site.register(AboutCollege,AboutCollegeAdmin)