from django.shortcuts import render
from department.models import Department
from .models import AboutCollege,GoverningBody
from django.core.urlresolvers import reverse


# Create your views here.

def home(request):
    departments = Department.objects.filter(popular = 'Yes')
    about_college = AboutCollege.objects.all()
    contex = {
        'departments' : departments,
        'about_college' : about_college.last()
    }
    return render(request, 'default_page/home.html', contex)


def view(request, slug):
    about_college= AboutCollege.objects.get(slug=slug)
    contex = {
        'about_college': about_college
    }
    return render(request, 'about_college/history_view.html', contex)


def about(request):
    persons= GoverningBody.objects.all()

    context= {
        'persons': persons
    }

    return render(request, 'about_college/about_us.html', context)


def about_us_view(request, slug):
    person= GoverningBody.objects.get(slug=slug)
    context= {
        'person': person
    }
    return render(request, 'about_college/about_us_single_view.html', context )



# def get_absolute_url(self):
#     return reverse('view', kwargs={'slug': self.slug})


