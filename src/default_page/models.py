from django.core.validators import URLValidator
from django.db import models
from django.template.defaultfilters import slugify


# Create your models here.

class AboutCollege(models.Model):
    title = models.CharField(max_length= 100)
    description = models.TextField()
    slug = models.SlugField(editable=False)
    height = models.IntegerField()
    width = models.IntegerField()
    upload_image = models.ImageField(height_field='height', width_field='width',upload_to='img/about_college')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    def __unicode__(self):
        return self.slug

    def save(self):
        self.slug = '%s' % (
            slugify(self.title)
        )
        super(AboutCollege, self).save()


class GoverningBody(models.Model):
    role = (
        ('Chairman', 'Chairman'),
        ('Principle', 'Principle'),
        ('Vice-Principle', 'vice-Principle'),

    )

    name = models.CharField(max_length=50)
    role = models.CharField(max_length=25, choices=role)
    short_description = models.CharField(max_length=200)
    details = models.TextField()
    slug = models.SlugField(editable=False)
    height = models.IntegerField(blank=True)
    width = models.IntegerField(blank=True)
    upload_image = models.ImageField(height_field='height', width_field='width', upload_to='img/governing_body')
    facebook_profile = models.URLField(blank=True, null=True, validators=[URLValidator()])
    date_of_birth = models.DateField(auto_now=False, auto_now_add=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def save(self):
        self.slug = '%s' % (
            slugify(self.name)
        )
        super(GoverningBody, self).save()


